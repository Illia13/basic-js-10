"use strict";
// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#".
// Додайте цей елемент в footer після параграфу.

const a = document.createElement("a");
a.textContent = "Learn More";
a.setAttribute("href", "#");

const footer = document.querySelector("footer");
footer.appendChild(a);

//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const select = document.createElement("select");
select.setAttribute("id", "rating");
const main = document.querySelector("main");
const features = main.querySelector("section");
main.insertBefore(select, features);

const option4 = document.createElement("option");
option4.value = "4";
option4.textContent = "4 Stars";
select.appendChild(option4);

const option3 = document.createElement("option");
option3.value = "3";
option3.textContent = "3 Stars";
select.appendChild(option3);

const option2 = document.createElement("option");
option2.value = "2";
option2.textContent = "2 Stars";
select.appendChild(option2);

const option1 = document.createElement("option");
option1.value = "1";
option1.textContent = "1 Stars";
select.appendChild(option1);
